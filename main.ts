import { CarEntity } from "./src/entity/car.entity";
import { SimplePresenter } from "./src/presenter/simple_presenter";
import { EmployeeRepo } from "./src/repository/employee.repo";
import { FuelRepo } from "./src/repository/fuel.repo";
import { CarSizeRateRepo } from "./src/repository/size_rate.repo";
import { CarParkService } from "./src/service/carpark.service";
import { EmployeeService } from "./src/service/employee.service";
import { GasFuelService } from "./src/service/gas_fuel.service";
import { InvoiceService } from "./src/service/invoice.service";
import { ParkingRateService } from "./src/service/parking_rate.service";

/**
 * This part initiating service implementations. 
 * Depending on what framework we use, each framework provides its own dependency injection logics (defining component or module)
 * I don't use any framework dependency during this test so my code shows how basic DI works
 */
const employeeService = new EmployeeService(new EmployeeRepo());
const fuelService = new GasFuelService(new FuelRepo());
const carSizeRateService = new ParkingRateService(new CarSizeRateRepo());
const invoiceService = new InvoiceService();

function main(): void {
    /**
     * Presenter will be an I/O endpoint. If it is web API controller, if a client application it can be a view presenter.
     */
    const presenter = new SimplePresenter(new CarParkService(employeeService, fuelService, carSizeRateService, invoiceService));
    const invoices = presenter.batchCarParking(input);
    console.log(invoices);
}

const input = [{
    "licencePlate": "A",
    "size": "large",
    "fuel": {
        "capacity": 57,
        "level": 0.07
    }
    }, {
    "licencePlate": "B",
    "size": "large",
    "fuel": {
        "capacity": 66,
        "level": 0.59
    }
    }, {
        "licencePlate": "C",
        "size": "large",
        "fuel": {
            "capacity": 54,
            "level": 0.49
        }
    }, {
        "licencePlate": "D",
        "size": "large",
        "fuel": {
            "capacity": 79,
            "level": 0.93
        }
    }, {
        "licencePlate": "E",
        "size": "large",
        "fuel": {
            "capacity": 94,
            "level": 0.2
        }
    }, {
        "licencePlate": "F",
        "size": "large",
        "fuel": {
            "capacity": 57,
            "level": 0.1
        }
    }, {
        "licencePlate": "G",
        "size": "small",
        "fuel": {
            "capacity": 56,
            "level": 0.05
        }
    }, {
        "licencePlate": "H",
        "size": "small",
        "fuel": {
            "capacity": 61,
            "level": 0.78
        }
    }, {
        "licencePlate": "I",
        "size": "small",
        "fuel": {
            "capacity": 60,
            "level": 0.65
        }
    }, {
        "licencePlate": "J",
        "size": "large",
        "fuel": {
            "capacity": 63,
            "level": 0.01
        }
}];

main();