import { EmployeeEntity } from "../entity/employee.entity";
import { IEmployeeRepo } from "./employee.repo.i";

export class EmployeeRepo implements IEmployeeRepo {
    /**
     * the branch is an example how finding employees of a certain car park in the real use case.
     * @param branch 
     * @returns 
     */
    public findAll(branch: string): EmployeeEntity[] {
        return [
            {
                id: 'A',
                commissionRate: 0.11
            },
            {
                id: 'B',
                commissionRate: 0.15
            }
        ]
    }
}