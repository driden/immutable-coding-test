import { FuelEntity } from "../entity/fuel.entity";
import { IFuelRepo } from "./fuel.repo.i";

export class FuelRepo implements IFuelRepo {
    /**
     * 
     * @param type this is an extension example of if fuel type is declared in Car entity, a car may run by gas or electricity
     * @returns 
     */
    public findOne(type: string = 'gas'): FuelEntity {
        if (type === 'gas') {
            return {
                type: 'gas',
                rate: 1.75
            };
        } else {
            throw new Error(`Fuel Type ${type} is not found in the repo`);
        }
    }
}