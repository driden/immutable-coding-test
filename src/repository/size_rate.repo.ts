import { SizeEntity } from "../entity/size.entity";
import { ICarSizeRateRepo } from "./size_rate.repo.i";

export class CarSizeRateRepo implements ICarSizeRateRepo {
    /**
     * gives a size categories
     * @returns 
     */
    public findAll(): SizeEntity[] {
        return [
            {
                size: 'small',
                rate: 25
            },
            {
                size: 'large',
                rate: 35
            }
        ];
    }
}