import { CarEntity } from "../entity/car.entity";
import { InvoiceEntity } from "../entity/invoice.entity";
import { ICarParkService } from "../service/carpark.service.i";
import { IPresenter } from "./presenter.i";



export class SimplePresenter implements IPresenter {

    public constructor(private carParkService: ICarParkService) {

    }

    public batchCarParking(cars: CarEntity[]): InvoiceEntity[] {
        return this.carParkService.processParking(cars);
    }
}