import { CarEntity } from "src/entity/car.entity";

export interface IPresenter {
    batchCarParking(cars: CarEntity[]): any[];
}