export interface IInvoiceService {
    createInvoice(licencePlate: string, parkingFee: number, fuelAdded: number, fuelingFee: number, employee: any): any;
}