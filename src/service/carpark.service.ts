import { CarEntity } from "../entity/car.entity";
import { FuelResultEntity } from "../entity/fuel_result.entity";
import { InvoiceEntity } from "../entity/invoice.entity";
import { ICarParkService } from "./carpark.service.i";
import { IEmployeeService } from "./employee.service.i";
import { IFuelService } from "./fuel.service.i";
import { IInvoiceService } from "./invoice.service.i";
import { IParkingRateService } from "./parking_rate.service.i";

export class CarParkService implements ICarParkService {
    /**
     * Each service must be injected from outside of this scope
     * @param employeeService 
     * @param fuelService 
     * @param parkingRateService 
     * @param invoiceService 
     */
    public constructor(
        private employeeService: IEmployeeService, 
        private fuelService: IFuelService,
        private parkingRateService: IParkingRateService,
        private invoiceService: IInvoiceService
    ) {}

    /**
     * When a car array is added, return an invoice array.
     * @param cars 
     * @returns 
     */
    public processParking(cars: CarEntity[]): InvoiceEntity[] {
        return cars.map(car => {
            const _car = car as CarEntity;
            const nextEmployee = this.employeeService.getNextEmployee();
            const parkingFee = this.parkingRateService.parkCar(_car);
            const fuelResult = this.fuelService.fuelCar(_car) as FuelResultEntity;
            this.employeeService.sumProfit(nextEmployee, (parkingFee + fuelResult.fuelPrice));
            return this.invoiceService.createInvoice(_car.licencePlate, parkingFee, fuelResult.requiredFuelAmount, fuelResult.fuelPrice, nextEmployee);
        });
    }
}