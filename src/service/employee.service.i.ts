export interface IEmployeeService {
    getNextEmployee(): any;
    sumProfit(employee: any, paidAmount: number): void;
}