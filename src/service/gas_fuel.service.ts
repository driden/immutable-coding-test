import { CarEntity } from "../entity/car.entity";
import { FuelEntity } from "../entity/fuel.entity";
import { FuelResultEntity } from "../entity/fuel_result.entity";
import { IFuelRepo } from "../repository/fuel.repo.i";
import { IFuelService } from "./fuel.service.i";

export class GasFuelService implements IFuelService {

    public constructor(private fuelRepo: IFuelRepo){}

    /**
     * calculate total fuel added, and total price
     * @param car 
     * @returns 
     */
    public fuelCar(car: CarEntity): FuelResultEntity {
        if (car.fuel.level >= 0.10) {
            const requiredFuelAmount = car.fuel.capacity - (car.fuel.capacity * car.fuel.level);
            const fuelRate: FuelEntity = this.fuelRepo.findOne('gas');
            const fuelPrice = requiredFuelAmount * fuelRate.rate;
            return {requiredFuelAmount, fuelPrice};
        } else return {requiredFuelAmount: 0, fuelPrice: 0};
    }
}