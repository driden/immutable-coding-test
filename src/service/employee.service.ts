import { EmployeeEntity } from "../entity/employee.entity";
import { IEmployeeRepo } from "../repository/employee.repo.i";
import { IEmployeeService } from "./employee.service.i";

export class EmployeeService implements IEmployeeService {
    private employees: EmployeeEntity[];
    private currentProfits: any = {};

    public constructor(private employeeRepo: IEmployeeRepo) {
        this.employees = this.employeeRepo.findAll();
        this.employees.map(employee => {
            const _employee = employee as EmployeeEntity;
            this.currentProfits[_employee.id] = 0;
        });
    }
    /**
     * check who has the minimum profit
     * @returns 
     */
    public getNextEmployee(): EmployeeEntity {
        let _lowProfitEmployee: EmployeeEntity;
        for (let i = 0; i < this.employees.length;  i++) {
            if (typeof _lowProfitEmployee === 'undefined' || this.currentProfits[_lowProfitEmployee.id] > this.currentProfits[this.employees[i].id]) {
                _lowProfitEmployee = this.employees[i];
            }
        }
        return _lowProfitEmployee;
    }
    /**
     * sum total profit of the employee
     * @param employee 
     * @param paidAmount 
     */
    public sumProfit(employee: EmployeeEntity, paidAmount: number): void {
        this.currentProfits[employee.id] += (paidAmount * employee.commissionRate);
    }
}