import { CarEntity } from "../entity/car.entity";
import { SizeEntity } from "../entity/size.entity";
import { ICarSizeRateRepo } from "../repository/size_rate.repo.i";
import { IParkingRateService } from "./parking_rate.service.i";

export class ParkingRateService implements IParkingRateService {
    private rateMap: any = {};
    /**
     * when initiate an instance, convert the array of sizes to dictionary style.
     * @param sizeRateRepo 
     */
    public constructor(private sizeRateRepo: ICarSizeRateRepo) {
        this.sizeRateRepo.findAll().map(item => {
            const _item = item as SizeEntity;
            this.rateMap[_item.size] = _item.rate;
        });
    }
    /**
     * it easily return the parking rate by the size
     * @param car 
     * @returns 
     */
    public parkCar(car: CarEntity): number {
        const rate = this.rateMap[car.size];
        if (typeof rate === 'number') return rate;
        // We may have an option to accept a false case of a size not in the sizeRateRepo. 
        // If accept, any unknown size from input will be treated as "large" for example.
        else throw new Error(`The car ${car.licencePlate} size is not specified (was ${car.size})`);
    }
}