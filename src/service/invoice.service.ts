import { EmployeeEntity } from "../entity/employee.entity";
import { InvoiceEntity } from "../entity/invoice.entity";
import { IInvoiceService } from "./invoice.service.i";

export class InvoiceService implements IInvoiceService {
    public createInvoice(licencePlate: string, parkingFee: number, fuelAdded: number, fuelingFee: number, employee: EmployeeEntity): InvoiceEntity {
        return {
            licencePlate: licencePlate,
            employee: employee.id,
            fuelAdded: fuelAdded,
            price: parkingFee + fuelingFee
        };
    }
}