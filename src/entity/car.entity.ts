export interface CarEntity {
    licencePlate: string;
    size: string;
    fuel: FuelMeter;
}

interface FuelMeter {
    capacity: number;
    level: number;
}