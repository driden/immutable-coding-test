export interface InvoiceEntity {
    licencePlate: string;
    employee: string;
    fuelAdded: number;
    price: number;
}