export interface FuelResultEntity {
    requiredFuelAmount: number;
    fuelPrice: number;
}