export interface SizeEntity {
    size: string;
    rate: number;
}