export interface EmployeeEntity {
    id: string;
    commissionRate: number;
}