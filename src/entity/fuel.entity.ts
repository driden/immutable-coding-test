export interface FuelEntity {
    type: string;
    rate: number;
}