# Immutable Coding Test

## Question
Design a parking service system. The service offers parking in addition to refueling to vehicles that require it, there are two employees who work on comission and get paid different rates. The system is responsible for assigning the workload equally between the two employees in a way that favours profit.

Small cars pay a flat rate of $25 for parking and large vehicles pay $35.
Every car with 10% or less fuel, will be refueled to maximum capacity and charged the fuel amount in addition to the parking fee.
Employee A gets paid 11% commission over the final amount paid, while employee B gets paid 15%.
Fuel has a fixed rate of $1.75/litre.

## How to use

### requirement
- node version `^14.14`
- npm version `^6.0.0`

### install modules
```sh
npm install
```

### build
```sh
npm run build
```

### execute
```sh
npm run start
```